CREATE DATABASE  IF NOT EXISTS `smartonline` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `smartonline`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: smartonline
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mobiles`
--

DROP TABLE IF EXISTS `mobiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mobiles` (
  `m_id` int(11) NOT NULL,
  `mobile_items` varchar(45) NOT NULL,
  `e_id` int(100) NOT NULL,
  `m_features` varchar(100) DEFAULT NULL,
  `m_price` varchar(100) DEFAULT NULL,
  `m_warenty` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`m_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobiles`
--

LOCK TABLES `mobiles` WRITE;
/*!40000 ALTER TABLE `mobiles` DISABLE KEYS */;
INSERT INTO `mobiles` VALUES (615,'i Phone',606,'4GB 1200mah 5.5\" screen block ','65000','2 year'),(616,'samsung',606,'4GB 1200mah 5.5\" screen block ','32000','1 year'),(617,'Mi Readme',606,'2GB 1200mah 5\" screen red','12000','1 year'),(618,'Real me',606,'3GB 1200mah 5.2\"\" screen gold','18000','1 year'),(619,'Note 1+',606,'4GB 1400mah 6.2\" screen black','90000','2 year'),(620,'Nokia',606,'2GB 1600mah 4.5\" screen red','15000','1 year'),(621,'Vivo',606,'4GB 1400mah 4.2\" screen black','16500','1 year'),(622,'Oppo',606,'2GB 1200mah 5.6\" screen black','16500','1 year'),(623,'Infonix',606,'4GB 1300mah 6.5\" screen gray','21500','1 year'),(624,'Honor',606,'2GB 1200mah 5.8\" screen black','18500','1 year'),(625,'Asus',606,'3GB 1200mah 5.6\" screen black','16800','1 year'),(626,'Coolpad',606,'4GB1200mah 5\" screen black','21550','1 year'),(627,'Panasonic',606,'2GB 1300mah 5.3\" screen black','13600','1 year'),(628,'LG',606,'2GB 1200mah 5.4\" screen black','18600','1 year');
/*!40000 ALTER TABLE `mobiles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-17 15:18:56
