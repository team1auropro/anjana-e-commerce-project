CREATE DATABASE  IF NOT EXISTS `smartonline` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `smartonline`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: smartonline
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `laptops`
--

DROP TABLE IF EXISTS `laptops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `laptops` (
  `l_id` int(11) NOT NULL,
  `l_items` varchar(45) NOT NULL,
  `e_id` int(11) NOT NULL,
  `l_features` varchar(45) NOT NULL,
  `l_price` int(11) NOT NULL,
  `l_warenty` varchar(45) NOT NULL,
  PRIMARY KEY (`l_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laptops`
--

LOCK TABLES `laptops` WRITE;
/*!40000 ALTER TABLE `laptops` DISABLE KEYS */;
INSERT INTO `laptops` VALUES (629,'Apple',607,'i5 8th GEN 16GB SSD 250  22\"',156000,'3 years'),(630,'Lenovo',607,'i3 5th GEN 8GB 500  22\"',56000,'1 year'),(631,'Dell',607,'i5 8th GEN 8GB  250  26\"',52000,'2 years'),(632,'Acer',607,'i3 4th GEN 4GB  250  20\"',42000,'1 year'),(634,'Hp',607,'i5 8th GEN 16GB  250  22\"',82000,'2 years'),(635,'Sony',607,'i3 5th GEN 16GB  500 28\"',42000,'1 year'),(636,'Micro soft',607,'i3 8thGEN 8GB 500 26\"',38000,'1 year'),(637,'Thoshiba',607,'i3 5th GEN 16GB  500 28\"',56000,'1 year'),(638,'MSI',607,'i3 5th GEN 16GB  500 28\"',48500,'1 year'),(639,'I Ball',607,'i5 8th GEN 8GB  250  26\"',39800,'2 year'),(640,'Lava',607,'i3 5th GEN 16GB  500 28\"',49000,'1 year');
/*!40000 ALTER TABLE `laptops` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-17 15:18:54
